<?php if (!defined('VB_ENTRY')) die('Access denied.');
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.3.0 - Licence Number 68628f15
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2017 vBulletin Solutions Inc. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

/**
 * @package vBDatabase
 */

/**
 * Mysql specific query interface -- see base class
 * @package vBDatabase
 */

class vB_dB_MYSQL_Query extends vB_dB_Query
{
	protected $db_type = 'MYSQL';
}

/*=========================================================================*\
|| #######################################################################
|| # Downloaded: 09:19, Mon May 22nd 2017
|| # CVS: $RCSfile$ - $Revision: 87198 $
|| #######################################################################
\*=========================================================================*/
