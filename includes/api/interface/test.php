<?php if (!defined('VB_ENTRY')) die('Access denied.');
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.3.0 - Licence Number 68628f15
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2017 vBulletin Solutions Inc. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

class Api_Interface_Test extends Api_Interface_Collapsed
{
	public function __construct()
	{
		// in collapsed form, we want to be able to load API classes
		$core_path = vB5_Config::instance()->core_path;
		vB5_Autoloader::register($core_path);

		vB::init();
		$request = new vB_Request_Test(
			array(
				'userid' => 1,
				'ipAddress' => '127.0.0.1',
				'altIp' => '127.0.0.1',
				'userAgent' => 'CLI'
			)
		);
		vB::setRequest($request);
		$request->createSession();
	}
}

/*=========================================================================*\
|| #######################################################################
|| # Downloaded: 09:19, Mon May 22nd 2017
|| # CVS: $RCSfile$ - $Revision: 83435 $
|| #######################################################################
\*=========================================================================*/
